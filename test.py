#This script checks if java exists
#Checks java version
#If java not available,it downloads and install java
#Provide users the capability to update the Java version
#And provides status by email

#Note - The script considers that smtp server is installed with mailx utility on the system and the script is run by user with sudo access to the system. The script is run using python3.7 version.

import os
import subprocess as sp
 
output = sp.getoutput('java -version')
if (os.system("java -version >/dev/null 2>&1")==0):
    print ("Java exits on system")
    print (output)
else:
    print ("Java does not exists on system. Downloading Java now..")
    os.system("sudo apt install default-jre")
    javaVersion = input("Please enter the Java version (E.g. openjdk-8-jdk) you would like to update:")
    print("Java version is: " + javaVersion)
    os.system("sudo apt install %s"%javaVersion)
    print("Creating alias..")
    email = input("Please enter the email address for notification:")
    sp.call(["mailx", "-s", "\"Java has been installed on the system\"", "%email"])
